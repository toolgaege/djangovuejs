from rest_framework import serializers
from .models import TwitItem

class TwitItemSerializer(serializers.ModelSerializer):
    text = serializers.CharField(max_length=50)
    name = serializers.CharField(max_length=50)
    created_at = serializers.DateField(format=None,input_formats=None,required=False)
    updated_at = serializers.DateField(format=None,input_formats=None,required=False)

    class Meta:
        model = TwitItem
        fields = ('__all__')