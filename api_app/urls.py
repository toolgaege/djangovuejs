from django.urls import path
from .views import TwitItemViews

urlpatterns = [
    path('twit-items/', TwitItemViews.as_view()),
    path('twit-items/<int:id>', TwitItemViews.as_view())
]