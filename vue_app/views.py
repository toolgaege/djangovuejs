from django.shortcuts import render

# Create your views here.
# vue_app/views.py
from django.shortcuts import render
from django.middleware import csrf

def index(request):
    token = csrf.get_token(request)
    c = {"csrftoken":token}
    return render(request, 'vue_app/index.html',c)