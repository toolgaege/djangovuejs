## virtual env create
     pip3 install virtualenv
     
## virtual env bring folder
     virtualenv venv

## virtual env activate
     venv\Scripts\activate 
      
## installation
    pip install django
    pip install rest_framework
    or 
    pip install -r requirements.txt
## run to web
    python manage.py runserver
    
## create db
    python manage.py makemigrations 
    python manage.py migrate
    
## createsuperuser
    python manage.py createsuperuser